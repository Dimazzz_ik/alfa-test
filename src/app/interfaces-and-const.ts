export interface IBlockType {
  id: number;
  name: string;
}

export interface IBlock {
  title: string;
  image: string;
  description: string;
  typeId: number;
  isFavorite: boolean;
}

export const FAVORITE_TYPE_ID = -2;
export const ALL_ID = -1;

export const TYPES: IBlockType[] = [
  {
    id: ALL_ID,
    name: 'Все'
  },
  {
    id: FAVORITE_TYPE_ID,
    name: 'Избранное'
  },
  {
    id: 1,
    name: 'Бизнес'
  },
  {
    id: 2,
    name: 'Мероприятия'
  }
];

export const BLOCKS: IBlock[] = [
  {
    title: 'title1',
    image: 'image.jpg',
    description: 'description',
    typeId: 1,
    isFavorite: false
  },
  {
    title: 'title2',
    image: 'image.jpg',
    description: 'description',
    typeId: 2,
    isFavorite: false
  },
  {
    title: 'title3',
    image: 'image.jpg',
    description: 'description',
    typeId: 1,
    isFavorite: false
  },
  {
    title: 'title4',
    image: 'image.jpg',
    description: 'description',
    typeId: 1,
    isFavorite: false
  },
  {
    title: 'title5',
    image: 'image.jpg',
    description: 'description',
    typeId: 2,
    isFavorite: false
  },
  {
    title: 'title6',
    image: 'image.jpg',
    description: 'description',
    typeId: 1,
    isFavorite: false
  },
  {
    title: 'title7',
    image: 'image.jpg',
    description: 'description',
    typeId: 1,
    isFavorite: false
  },
  {
    title: 'title8',
    image: 'image.jpg',
    description: 'description',
    typeId: 2,
    isFavorite: false
  },
  {
    title: 'title9',
    image: 'image.jpg',
    description: 'description',
    typeId: 1,
    isFavorite: false
  },
  {
    title: 'title10',
    image: 'image.jpg',
    description: 'description',
    typeId: 1,
    isFavorite: false
  },
  {
    title: 'title11',
    image: 'image.jpg',
    description: 'description',
    typeId: 2,
    isFavorite: false
  },
];
