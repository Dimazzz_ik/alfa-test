import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {IBlock} from "../interfaces-and-const";

@Component({
    selector: 'app-block',
    templateUrl: './block.component.html',
    styleUrls: ['./block.component.scss']
})

export class BlockComponent implements OnInit {
    constructor() {
    }

    @Input() blockInfo: IBlock;
    @Output() detailsClick = new EventEmitter();

    ngOnInit() {

    }

    onDetailsClick() {
        this.detailsClick.emit(null);
    }

    toggleFavorite() {
        this.blockInfo.isFavorite = !this.blockInfo.isFavorite;
    }
}
