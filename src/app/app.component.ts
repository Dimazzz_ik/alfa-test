import {Component, OnInit} from '@angular/core';
import {IBlock, TYPES, BLOCKS, IBlockType, FAVORITE_TYPE_ID, ALL_ID} from "./interfaces-and-const";

@Component({
    selector: 'my-app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
    readonly PAGE_SIZE = 4;

    lastScroll: 0;

    types: IBlockType[] = [...TYPES];
    activeTypeId = -1;

    allBlocks: IBlock[] = [...BLOCKS];

    loadedBlocks: IBlock[] = [];
    showBlocks: IBlock[] = [];

    clickedBlock: IBlock;
    showModal = false;

    ngOnInit() {
        this.loadBlock();
    }

    filterBlocks(id?: number) {
        if (!id) {
            this.showBlocks = [...this.loadedBlocks];
            return;
        }

        switch (id) {
            case ALL_ID:
                this.showBlocks = [...this.loadedBlocks];
                break;
            case FAVORITE_TYPE_ID:
                this.showBlocks = this.loadedBlocks.filter(item => item.isFavorite);
                break;
            default:
                this.showBlocks = this.loadedBlocks.filter(item => item.typeId === id);
                break;
        }


        this.activeTypeId = id;
    }

    loadBlock() {
        if (!this.allBlocks.length || this.activeTypeId === FAVORITE_TYPE_ID) {
            return;
        }

        this.loadedBlocks.push(...this.allBlocks.splice(0, this.PAGE_SIZE));
        this.filterBlocks();
    }

    onContainerBlockScroll(event) {
        const elem = event.target;

        // Направление стролла в низ
        if (this.lastScroll < elem.scrollTop) {
            if (elem.offsetHeight + elem.scrollTop >= elem.scrollHeight) {
                this.loadBlock();
            }
        }

        this.lastScroll = elem.scrollTop;
    }

    openBlockInModal(block) {
        this.clickedBlock = block;
        this.showModal = true;
    }
}
